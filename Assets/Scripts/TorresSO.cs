using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class TorresSO : ScriptableObject
{
    public float dmg;
    public string type;
    public float atkSpeed;
    public float range;
    public Sprite spriteTorre;
    public GameObject proyectil;
}
