using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.EventSystems;

public class DragItems : MonoBehaviour
{
    Vector3 posicionincial;
    Vector3 posicioninicial2;
    bool nosepuede;
    RecursosManager recursos;
    bool uno=false;
    bool comprado;
    void Start()
    {
        if (this.transform.tag == "canon" || this.transform.tag == "ballesta" || this.transform.tag=="ballesta+" ||this.transform.tag=="canon+")
        {
            foreach(Transform child in this.transform.GetChild(1).GetChild(0))
            {
                if (child.tag == "radio")
                {
                    child.gameObject.SetActive(false);
                }else if (child.tag == "circulo")
                {
                   child.gameObject.SetActive(false);
                }
            }
        }
        recursos = GameObject.Find("Spaner").GetComponent<RecursosManager>();

    }

    private void Update()
    {
        if (recursos.obtenerpiedra() < 0)
        {
            recursos.SetPiedra(0);
        }
        else if (recursos.obtenirmadera() < 0)
        {
            recursos.SetMadera(0);
        }
    }
    public void OnMouseDrag()
    {
        Vector3 posicioratoli = new Vector3(Input.mousePosition.x, Input.mousePosition.y, 10);
        if ((this.transform.tag == "canon" && this.recursos.obtenerpiedra() >= 2 && this.recursos.obtenirmadera() >= 3) || (this.transform.tag == "ballesta" && this.recursos.obtenerpiedra() >= 5 && this.recursos.obtenirmadera() >= 6) ||(this.transform.tag == "ballesta+" && this.recursos.obtenerpiedra()>=7 && this.recursos.obtenirmadera()>=8) || (this.transform.tag == "canon+" && this.recursos.obtenerpiedra()>=4 && this.recursos.obtenirmadera()>=5))
        {
            foreach (Transform child in this.transform)
            {
                if (child.tag == "circulo")
                {
                    child.gameObject.SetActive(true);
                }
            }
            this.transform.position = Camera.main.ScreenToWorldPoint(posicioratoli);
        }

        
    }

    public void OnMouseEnter()
    {
       
        //this.GetComponent<SpriteRenderer>().color = Color.green;
    }

    public void OnMouseDown()
    {
        uno = false;
        posicionincial = transform.position;
        if (this.transform.tag == "canon")
        {
            posicioninicial2 = new Vector3(6.444794f, -2.854355f, 0f);
        }else if (this.transform.tag == "ballesta+")
        {
            posicioninicial2 = new Vector3(6.432f, -0.198f, 0.0f);

        }else if (this.transform.tag == "ballesta")
        {
            posicioninicial2 = new Vector3(8.034f, -2.887f, 0f);
        }else if (this.transform.tag == "canon+")
        {
            posicioninicial2 = new Vector3(8.01f, -0.26f, 0f);
        }
      

    }

    public void OnMouseUp()
    {

        if (!uno && comprado)
        {
            GameObject gameObject = Instantiate(this.gameObject);
            gameObject.transform.position = posicioninicial2;
            uno = true;
        }

        foreach (Transform child in this.transform.GetChild(1).GetChild(0))
            {
                if (child.tag == "radio")
                {
                    child.gameObject.SetActive(true);
                }
                else if (child.tag == "circulo")
                {
                    child.gameObject.SetActive(false);
                }
            }
           
            uno = false;
        if (nosepuede)
        {
            this.transform.position = posicionincial;
        }
        if (recursos.obtenirmadera() > 0 && recursos.obtenerpiedra() > 0 && comprado)
        {
            if (this.transform.tag == "canon")
            {
                recursos.SetMadera(recursos.obtenirmadera() - 3);
                recursos.SetPiedra(recursos.obtenerpiedra() - 2);
            }
            else if (this.transform.tag == "ballesta")
            {
                recursos.SetMadera(recursos.obtenirmadera() - 6);
                recursos.SetPiedra(recursos.obtenerpiedra() - 5);
            }else if (this.transform.tag == "ballesta+")
            {
                recursos.SetMadera(recursos.obtenirmadera() - 8);
                recursos.SetPiedra(recursos.obtenerpiedra() - 7);
            }else if (this.transform.tag == "canon+")
            {
                recursos.SetMadera(recursos.obtenirmadera() - 5);
                recursos.SetPiedra(recursos.obtenerpiedra() - 4);
            }
        
        }
        comprado = false;

    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "suelo")
        {
            comprado = true;
            nosepuede = false;
            //this.transform.position=collision.transform.position;
            //this.transform.GetChild(0).gameObject.GetComponent<SpriteRenderer>().color = Color.green;
        }
        else
        {
            nosepuede = true;
            comprado = false;
            //this.transform.GetChild(0).gameObject.GetComponent<SpriteRenderer>().color = Color.red;
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {

        if (collision.tag == "suelo")
        {
            nosepuede = true;
            //this.GetComponent<SpriteRenderer>().color = Color.red;
        }
    }
}