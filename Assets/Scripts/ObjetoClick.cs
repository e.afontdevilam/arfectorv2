using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.UI;

public class ObjetoClick : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
    }

    public void OnButtonClick()
    {
        Torre torre = FindAnyObjectByType<Torre>();
        this.transform.parent.parent.gameObject.SetActive(false);

        if (this.transform.tag == "SpeedUp")
        {
            torre.GetComponent<Torre>().atkSpeed /= 1.1f;

            
        }
        else if (this.transform.tag == "DamageUp")
        {
            torre.GetComponent<Torre>().dmg *= 1.2f;
       
        }
        else if (this.transform.tag == "RangeUp")
        {
            torre.GetComponent<Torre>().range *= 1.6f;
           
        }
    }
}
