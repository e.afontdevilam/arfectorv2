using System.Collections;
using System.Collections.Generic;
using TMPro;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.SceneManagement;

public class RecursosManager : MonoBehaviour
{
    public GameObject poolEnemigos;
    public delegate void subirnivel();
    public subirnivel lvlup;
    public float xp;
    public float xpMax;
    int rupias;
    int madera;
    int piedra;
    int seg;
    int lvl;
    int piedraelemntal;
    public GameObject[] textos;
    int enemigos;

    public int obtenirmadera()
    {
        return madera;
    }
    public int obtenerpiedra()
    {
        return piedra;
    }
    public int obtenirrupias()
    {
        return rupias;
    }
    void Start()
    {
        seg = 120;
        enemigos = 0;
        piedraelemntal = 0;
        madera = 6;
        piedra = 6;
        xp = 0;
        lvl = 1;
        xpMax = 100;
        StartCoroutine(spawner());
        StartCoroutine(tiempo());
        StartCoroutine(velup());
    }

    void Update()
    {
        if (this.xp >= this.xpMax)
        {
            Debug.Log("level up");
            levelup();
        }
        if (seg <= 0)
        {
            SceneManager.LoadScene("Victoria");
        }
        textos[0].GetComponent<TextMeshProUGUI>().text = ""+madera;
        textos[1].GetComponent<TextMeshProUGUI>().text = ""+piedra;
        textos[2].GetComponent<TextMeshProUGUI>().text = "" +rupias;
        textos[3].GetComponent<TextMeshProUGUI>().text = "Level " + lvl;
        textos[4].GetComponent<TextMeshProUGUI>().text = "TIME REMAINING: " + seg;
    }
    void levelup()
    {
        this.xp = 0;
        lvlup?.Invoke();
        this.lvl++;
    }

    IEnumerator tiempo()
    {
        while (true)
        {
            yield return new WaitForSeconds(1f);
            seg--;
        }
    }

    IEnumerator velup()
    {
        while (true)
        {
            yield return new WaitForSeconds(30f);
            Debug.Log("velup");
            for (int i = 0; i < poolEnemigos.transform.childCount; i++)
            { 
                poolEnemigos.transform.GetChild(i).GetComponent<Enemigo>().speed *= 1.2f;
                poolEnemigos.transform.GetChild(i).GetComponent<Enemigo>().vida *= 2;
            }
        }
    }

    IEnumerator spawner()
    {
        while (true)
        {
            yield return new WaitForSeconds(2f);
            for (int i = 0; i < poolEnemigos.transform.childCount; i++)
            {
                if(!poolEnemigos.transform.GetChild(i).gameObject.active)
                {
                    poolEnemigos.transform.GetChild(i).gameObject.SetActive(true);
                    enemigos++;
                    break;
                }
            }
        }   
    }

   
    public void SetMadera(int value)
    {
        madera = value;
    }

    public void SetPiedra(int value)
    {
        piedra = value;
    }

    public void SetRupias(int value)
    {
        rupias = value;
    }
}
