using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Torre : MonoBehaviour
{
    bool disparando;
    List<GameObject> enemics = new List<GameObject>();
    public TorresSO torre;
    public float dmg;
    public float atkSpeed;
    public float range;
    // Start is called before the first frame update
    void Start()
    {
        this.dmg = torre.dmg;
        this.atkSpeed = torre.atkSpeed;
        this.range = torre.range;

        this.transform.GetChild(0).GetComponent<CircleCollider2D>().radius = range;
        this.GetComponent<SpriteRenderer>().sprite = torre.spriteTorre;
        StartCoroutine(disparar());
    }

    // Update is called once per frame
    void Update()
    {
        if (enemics.Count > 0)
        {
            Vector2 direccio2 = enemics[0].transform.position - transform.parent.position;
            direccio2.Normalize();
            this.transform.right = -direccio2;
        }
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Duende")
        {
            enemics.Add(collision.gameObject);
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.tag == "Duende")
        {
            enemics.Remove(collision.gameObject);
        }
    }

    public IEnumerator disparar()
    {
        while (true)
        {
            yield return new WaitForSeconds(atkSpeed);
            if (enemics.Count > 0)
            {
                GameObject proj = Instantiate(torre.proyectil);
                Vector2 direccio = enemics[0].transform.position - transform.parent.position;
                direccio.Normalize();
                proj.transform.up = direccio;
                proj.transform.position = this.transform.parent.position;
                proj.GetComponent<Projectil>().dmg = dmg;
                proj.GetComponent<Rigidbody2D>().velocity = direccio * 10;

            }

            /*while (true)
            {
                yield return new WaitForSeconds(torre.atkSpeed);
                if (enemics.Count > 0)
                {
                    for (int i = 0; i < projectilPool.transform.childCount; i++)
                    {
                        Debug.Log(projectilPool.transform.childCount);
                        GameObject bala = projectilPool.transform.GetChild(i).gameObject;
                        if (!bala.active)
                        {
                            Debug.Log("disparo");
                            bala.gameObject.SetActive(true);
                            Vector2 direccio = enemics[0].transform.position - transform.parent.position;
                            direccio.Normalize();
                            bala.transform.up = direccio;
                            bala.transform.position = this.transform.parent.position;
                            bala.GetComponent<Projectil>().dmg = torre.dmg;
                            bala.GetComponent<Rigidbody2D>().velocity = direccio * 10;
                            break;
                        }
                    }
                }
            }*/

        }
    }
}
