using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Victoria : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(estelada());
    }

    // Update is called once per frame
    void Update()
    {   
    }

    IEnumerator estelada()
    {
        while (true)
        {
            yield return new WaitForSeconds(10f);
            SceneManager.LoadScene("Menu");
        }
    }
}
