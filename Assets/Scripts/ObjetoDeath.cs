using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ObjetoDeath : MonoBehaviour
{
    public GameObject[] objetos;
    public GameObject recursos;
    void Start()
    {
        recursos.GetComponent<RecursosManager>().lvlup += escogeritem;
        this.gameObject.GetComponent<Image>().color=Color.clear;
    }

    void Update()
    {
    }

    public void escogeritem()
    {
        this.gameObject.SetActive(true);

        List<GameObject> items = new List<GameObject>(objetos);

        for (int i = 0; i < 3; i++)
        {
            int rit = Random.Range(0, items.Count);
            this.gameObject.GetComponent<Image>().color = Color.white;
            GameObject objeto = Instantiate(items[rit]);
            objeto.transform.SetParent(this.transform, false);
           

            switch (i)
            {
                case 0:
                    //left
                    //objeto.GetComponent<RectTransform>().offsetMin = new Vector2(137.4153f, 160.4636f);
                    //objeto.GetComponent<RectTransform>().offsetMax = new Vector2(-609.01f, -160.4636f);
                    objeto.GetComponent<RectTransform>().anchorMin= new Vector2(0, 0.5f);
                    objeto.GetComponent<RectTransform>().anchorMax= new Vector2(0, 0.5f);
                    objeto.GetComponent<RectTransform>().pivot = new Vector2(0.5f, 0.5f);
                    objeto.GetComponent<RectTransform>().anchoredPosition = new Vector2(111, 0);
                    objeto.GetComponent<RectTransform>().sizeDelta = new Vector2(objeto.GetComponent<RectTransform>().sizeDelta.x, objeto.GetComponent<RectTransform>().sizeDelta.y);

                    break;
                case 1:
                    //right
                    //objeto.GetComponent<RectTransform>().offsetMin = new Vector2(620.63f, 160.4636f);
                    //objeto.GetComponent<RectTransform>().offsetMax = new Vector2(-157.99f, -160.4636f);
                    objeto.GetComponent<RectTransform>().anchorMin = new Vector2(1f, 0.5f);
                    objeto.GetComponent<RectTransform>().anchorMax = new Vector2(1f, 0.5f);
                    objeto.GetComponent<RectTransform>().pivot = new Vector2(0.5f, 0.5f);
                    objeto.GetComponent<RectTransform>().anchoredPosition = new Vector2(-111, 0);
                    objeto.GetComponent<RectTransform>().sizeDelta = new Vector2(objeto.GetComponent<RectTransform>().sizeDelta.x, objeto.GetComponent<RectTransform>().sizeDelta.y);
                    break;
                case 2:
                    //middle
                    //objeto.GetComponent<RectTransform>().offsetMin = new Vector2(396.21f, 160.4636f);
                    //objeto.GetComponent<RectTransform>().offsetMax = new Vector2(-382.41f, -160.4636f);
                    objeto.GetComponent<RectTransform>().anchorMin = new Vector2(0.5f, 0.5f);
                    objeto.GetComponent<RectTransform>().anchorMax = new Vector2(0.5f, 0.5f);
                    objeto.GetComponent<RectTransform>().pivot = new Vector2(0.5f, 0.5f);
                    objeto.GetComponent<RectTransform>().anchoredPosition = new Vector2(5, 0);
                    objeto.GetComponent<RectTransform>().sizeDelta = new Vector2(objeto.GetComponent<RectTransform>().sizeDelta.x, objeto.GetComponent<RectTransform>().sizeDelta.y);
                    break;
            }

            Debug.Log(objeto + " " + objeto.transform.position);
            items.RemoveAt(rit);
        }
    }
}
