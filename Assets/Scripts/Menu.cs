using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Menu : MonoBehaviour
{
    // Start is called before the first frame update
    public void PlayGame()
    {
        SceneManager.LoadScene("HOW TO PLAY");
    }

    public void quitGame()
    {
        Application.Quit();
    }

    //utilizada en otra escena xd.
    public void cambiaralvl1()
    {
        SceneManager.LoadScene("Nivel1");
    }

}
