using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class EnemigoSO : ScriptableObject
{
    public int hp;
    public float speed;
    public string type;
    public Sprite spriteEnemigo;
}
