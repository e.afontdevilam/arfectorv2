using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Enemigo : MonoBehaviour
{
    // Start is called before the first frame update
    public delegate void muerteenemigo();
    public muerteenemigo deathenemigo;
    public EnemigoSO enemigo;
    public GameObject[] puntos;
    private List<GameObject> camino;
    public RecursosManager recursosManager;
    public int vida;
    bool muerto = false;
    public float speed;

    void Start()
    {
        speed=enemigo.speed;
        vida = enemigo.hp;
        camino = new List<GameObject>(puntos);
        this.GetComponent<SpriteRenderer>().sprite = enemigo.spriteEnemigo;
    }

    // Update is called once per frame
    void Update()
    {
        Debug.Log("VidaEnemigo"+vida);
        Debug.Log("VidaEnemigoSO"+ enemigo.hp);
        if (vida <= 0 && !muerto)
        {
            muerto = true;
            deathenemigo?.Invoke();
            Debug.Log("MUELTO");
            dropEnemigo();
            ResetEnemigo();

        }
        Vector2 path = camino[0].transform.position - this.transform.position;
        path.Normalize();
        this.GetComponent<Rigidbody2D>().velocity = path * this.GetComponent<Enemigo>().speed;
        if ((this.transform.position.x <= camino[0].transform.position.x + 0.25f && this.transform.position.x >= camino[0].transform.position.x - 0.25f) && (this.transform.position.y <= camino[0].transform.position.y + 0.25f && this.transform.position.y >= camino[0].transform.position.y - 0.25f))
        {
            //Debug.Log("he llegado");
            if (camino[0].transform.tag == "final")
            {
                if (this.tag == "Duende")
                {
                    SceneManager.LoadScene("GameOver");
                    //cambio de escena a game over del jefe duende
                }
            }

            camino.RemoveAt(0);
        }
    }
    private void dropEnemigo()
    {
        recursosManager.xp += 10f;
        int rand = UnityEngine.Random.Range(1,3);
        int madera=recursosManager.obtenirmadera();
        int piedra= recursosManager.obtenerpiedra();
        recursosManager.SetMadera(madera += rand);
        recursosManager.SetPiedra(piedra += rand);
    }

    private void ResetEnemigo()
    {
        Debug.Log("ResetEnemigo");
        vida = enemigo.hp;
        this.gameObject.SetActive(false);
        this.gameObject.transform.position = new Vector3(-9.17f, -0.66f, 0);
        camino.Clear();
        camino = new List<GameObject>(puntos);
        muerto = false;
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        //CA��N
        if (collision.transform.tag == "Projectil")
        {
            this.GetComponent<SpriteRenderer>().color = Color.red;
            int randomValue = UnityEngine.Random.Range(1, 10);
            StartCoroutine(changecolor());

            if (randomValue == 10)
            {
                vida -= (int)collision.gameObject.GetComponent<Projectil>().dmg*2;
               // Debug.Log(vida);
            }
            else
            {
                vida -= (int)collision.gameObject.GetComponent<Projectil>().dmg;
                //Debug.Log(vida);
            }
        }
    }

    public IEnumerator changecolor()
    {
        yield return new WaitForSeconds(0.8f);
        this.GetComponent<SpriteRenderer>().color = Color.white;
    }
}
