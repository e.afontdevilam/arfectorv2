using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BarraXp : MonoBehaviour
{
    // Start is called before the first frame update
    public GameObject lvl;
    private RecursosManager recursosManager;
    void Start()
    {
        recursosManager = lvl.GetComponent<RecursosManager>();
    }

    // Update is called once per frame
    void Update()
    {
        float xp = recursosManager.xp*10;
        this.GetComponent<RectTransform>().sizeDelta = new Vector3 (xp, 39.16f, 0);
    }
}
