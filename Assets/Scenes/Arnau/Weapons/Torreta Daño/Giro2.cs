using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Giro2 : MonoBehaviour
{
    public Vector3 rotationSpeed = new Vector3(0, 0, -10);

    void Update()
    {
        transform.Rotate(rotationSpeed * Time.deltaTime);
    }
}
